# Combat system:

## What the player can do:
 - Select the element he want to use
 - Draw around the character to attack
   - **Knife**: line from top left to bottom right. Additional damages if the character is falling. Only one enemy?
   - **Thrust**: line from left to right. Knock back the enemy. Additional damages if the character is moving to the right. Multiple enemies
   - **Lift**: line from bottom left to top right. Lift the enemy up with little damage. Only one enemy?
   - **Fireball**: flies following the mouse when the fireball is launched. Then the fireball goes straight. Single enemy. How to launch it?
   - **Arrow**: follow the opposite direction of the mouse. Is thrown in parabola, faster than the fireball. Knock back the enemy. The enemy can be stuck in the wall. How to launch it? Only one enemy?
 - Draw anywhere:
   - **Wood**: attack anywhere on the screen, no range limit. How to launch it? Multiple enemies?
 - Shield
   - **Soil**: draw a shield to absorb the attacks. Is transparent at first and appear for a short time after draw.

## An attack should have:
 - A range (distance from the player to hit the monster)
 - A value (quantity of damage it deals). Should it be somehow random?
 - Particular effect (knock back) with its value

*Note*: Instead of Attack, we can maybe speak about Action, in order to include the Shield in it

We can also do a more generic way (would be better):
 - Having a draw pattern
 - Checking if the user action follow one of the pattern
 - If yes, attack, else do nothing

In that way, we'll need a way to store that. We can use JSON and load one time at the beginning of the game, to create all the possible attacks.

In both ways, an attack will have to be connected to the enemy (or enemies) it attack.

## Others ideas:
 - Have a different delay before being able to reuse the attack?
 - A combo increase the power of the attacks
 - We create a combo when we attack quickly using different attacks (> 2)

## Implementation ideas:

For the attacks to be able to change the enemy behavior, the two implementations will have to be connected somehow.
 -> Maybe creation of a EnemyManager if necessary
The attack system may be separated from the main file for the character to clarify.
The controls may stay in the character script.
Maybe creation of an object Attack to handle better the attack

## Validity of an attack:

An attack is valid if it's drawn correctly, or almost (with a variable to adjust)

## Questions:

Foreswing/backswing: what it is exactly?
For me a backswing is a bad effect after an attack. In that case, why do we speak about animation?

## What will be needed:

Images for the attacks. At first, at least one set of image for one attack to be able to implement the animation.
